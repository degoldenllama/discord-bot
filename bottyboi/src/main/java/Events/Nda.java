package Events;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Nda extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent EventTime) {

        String messege2Sent = EventTime.getMessage().getContentRaw();
        if (messege2Sent.equalsIgnoreCase("!nda")) {

            EventTime.getChannel().sendMessage("https://docs.google.com/document/d/1a0YOyXD3v1D4YpF5JzwSVKkn60ea_ZeMoyF65HPN3Vw/edit?usp=sharing\n" +
                    "Please sign this as soon as possible and send it to one of the leads! THANKS!!").queue();
        }
    }
}