package Events;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Time extends ListenerAdapter{
    public void onGuildMessageReceived(GuildMessageReceivedEvent EventTime){

        String messege2Sent = EventTime.getMessage().getContentRaw();
        if(messege2Sent.equalsIgnoreCase("!time")){

            TimeZone timeZone1 = TimeZone.getTimeZone("US/Pacific");
            TimeZone timeZone2 = TimeZone.getTimeZone("US/Mountain");
            TimeZone timeZone3 = TimeZone.getTimeZone("US/Central");
            TimeZone timeZone4 = TimeZone.getTimeZone("US/Eastern");
            TimeZone timezone5 = TimeZone.getTimeZone("Europe/London");
            TimeZone timezone8 = TimeZone.getTimeZone("Europe/Moscow");
            TimeZone timeZone7 = TimeZone.getTimeZone("Asia/Kolkata");
            TimeZone timeZone6 = TimeZone.getTimeZone("Europe/Helsinki");
            TimeZone timeZone9 = TimeZone.getTimeZone("CET");
            TimeZone timeZone10 = TimeZone.getTimeZone("GMT+9");
            TimeZone timeZone11 = TimeZone.getTimeZone("GMT+8");
            TimeZone timeZone12 = TimeZone.getTimeZone("GMT+8");

            Calendar time1 = new GregorianCalendar();
            Calendar time2 = new GregorianCalendar();
            Calendar time3 = new GregorianCalendar();
            Calendar time4 = new GregorianCalendar();
            Calendar time5 = new GregorianCalendar();
            Calendar time6 = new GregorianCalendar();
            Calendar time7 = new GregorianCalendar();
            Calendar time8 = new GregorianCalendar();
            Calendar time9 = new GregorianCalendar();
            Calendar time10 = new GregorianCalendar();
            Calendar time11 = new GregorianCalendar();
            Calendar time12 = new GregorianCalendar();

            time1.setTimeZone(timeZone1);
            time2.setTimeZone(timeZone2);
            time3.setTimeZone(timeZone3);
            time4.setTimeZone(timeZone4);
            time5.setTimeZone(timezone5);
            time6.setTimeZone(timeZone6);
            time7.setTimeZone(timeZone7);
            time8.setTimeZone(timezone8);
            time9.setTimeZone(timeZone9);
            time10.setTimeZone(timeZone10);
            time11.setTimeZone(timeZone11);
            time12.setTimeZone(timeZone12);
/*
// (fixing less then 10 minute bug)
            if(time1.MINUTE>10){
                time1.MINUTE = ("0"+time1.MINUTE);
            }
*/
            EventTime.getChannel().sendMessage(" " +
                    "(NA) Pacific Standard Time "+time1.get(time1.HOUR_OF_DAY)+":" +time1.get(time1.MINUTE)+
                    "\n"+" (NA) Mountain Standard Time "+time2.get(time2.HOUR_OF_DAY)+":" +time2.get(time2.MINUTE)+
                    "\n"+" (NA) Central Time "+time3.get(time3.HOUR_OF_DAY)+":" +time3.get(time3.MINUTE)+
                    "\n"+" (NA) Eastern Standard Time "+time4.get(time4.HOUR_OF_DAY)+":" +time4.get(time4.MINUTE)+
                    "\n"+" (EN) British Summer Time "+time5.get(time5.HOUR_OF_DAY)+":" +time5.get(time5.MINUTE)+
                    "\n"+" (EU) Central Europe "+time9.get(time9.HOUR_OF_DAY)+":" +time9.get(time9.MINUTE)+
                    "\n"+" (EU) Finland "+time6.get(time6.HOUR_OF_DAY)+":" +time6.get(time6.MINUTE)+
                    "\n"+" (RS) Western Mother Russia"+time8.get(time8.HOUR_OF_DAY)+":" +time8.get(time8.MINUTE)+
                    "\n"+" (In) India "+time7.get(time7.HOUR_OF_DAY)+":" +time7.get(time7.MINUTE)+
                    "\n"+" (PH) Philippines "+time11.get(time11.HOUR_OF_DAY)+":" +time11.get(time11.MINUTE)+
                    "\n"+" (RS) Eastern Mother Russia "+time10.get(time10.HOUR_OF_DAY)+":" +time10.get(time10.MINUTE)+
                    "\n"+" (Aus) Kangaroo Land "+time12.get(time12.HOUR_OF_DAY)+":"+time12.get(time12.MINUTE)
            ).queue();

        }

    }

}