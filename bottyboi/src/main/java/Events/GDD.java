package Events;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GDD extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent EventTime) {

        String messege2Sent = EventTime.getMessage().getContentRaw();
        if (messege2Sent.equalsIgnoreCase("!GDD")) {

            EventTime.getChannel().sendMessage("The initial game idea is one that takes place in a fantasy setting set in the 16th century (primarily focused on the tech during this time line not the setting) " +
                    "the game itself will be set in an open sandbox setting with a story (similar to shadow of war where you can play the main story and quests or simply cruise by them and once you finish" +
                    " the quests you can continue playing) there will be multiple factions that you can choose to side with to unlock faction specific gear/skills and new lore, each faction will have varying" +
                    " backgrounds but non will be outright good or bad once you invest time in them to learn their lore, you will be able to recruit companions that are fully voiced and you can create your own" +
                    " town (scale is still pretty up in the air) but this will allow you to create armies and create a hybrid city that has parts of various factions (for example you choose to work with" +
                    "shape~shifters and paladins and can create a unique hybrid troop to put in your army after you earn enough rep) there will be events to defend your town and large "+"world bosses"+
                    "will be able to spawn to help keep late game interesting and we could potentially add in a PvP or Coop mode to enable players to really focus on crafting unique builds to test into " +
                    "late game similar to dark souls, the setting will also be on the darker side but won't be grim dark. We'll have areas that are lush and beautiful but also some that are dark and eerie."+
                    "With all this we will be keeping it simple still. Our first goal for this new project will be to focus strictly on the gameplay, squad management, and character customization.\n  https://docs.google.com/document/d/1fE4flRIFMGlRAegkI5stEN6X8in4Mme1bYjy6a_diGo/edit#heading=h.czfz5p91e9om").queue();

        }
    }
}
